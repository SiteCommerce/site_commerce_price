<?php

namespace Drupal\site_commerce_price\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the group price field type.
 *
 * @FieldType(
 *   id = "site_commerce_price_group",
 *   label = @Translation("Group price"),
 *   description = @Translation("Stores information about the decimal value of the group price, currency code, price group, and price notes."),
 *   category = @Translation("SiteCommerce"),
 *   default_widget = "site_commerce_price_group_default",
 *   default_formatter = "site_commerce_price_group_default"
 * )
 */
class PriceGroupItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    return [
      'available_currencies' => [],
      'all_currencies' => $currency_codes,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    $element = [];
    $element['available_currencies'] = [
      '#type' => 'checkboxes',
      '#title' => new TranslatableMarkup('Available currencies'),
      '#description' => new TranslatableMarkup('If no currencies are selected, all currencies will be available.'),
      '#options' => array_combine($currency_codes, $currency_codes),
      '#default_value' => $this->getSetting('available_currencies'),
      '#multiple' => TRUE,
      '#size' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['group'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price group'))
      ->setRequired(FALSE);

    $properties['prefix'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price prefix'))
      ->setRequired(FALSE);

    $properties['suffix'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Price suffix'))
      ->setRequired(FALSE);

    $properties['number_from'] = DataDefinition::create('float')
      ->setLabel(new TranslatableMarkup('Price from'))
      ->setRequired(FALSE);

    $properties['number'] = DataDefinition::create('float')
      ->setLabel(new TranslatableMarkup('Price'))
      ->setRequired(FALSE);

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Currency'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'group' => [
          'description' => new TranslatableMarkup('Price group'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'prefix' => [
          'description' => new TranslatableMarkup('Price prefix'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'suffix' => [
          'description' => new TranslatableMarkup('Price suffix'),
          'type' => 'varchar',
          'length' => 255,
          'not null' => FALSE,
          'default' => '',
        ],
        'number_from' => [
          'description' => new TranslatableMarkup('Price from'),
          'type' => 'numeric',
          'precision' => 65,
          'scale' => 30,
          'unsigned' => TRUE,
          'not null' => FALSE,
          'default' => 0,
        ],
        'number' => [
          'description' => new TranslatableMarkup('Price'),
          'type' => 'numeric',
          'precision' => 65,
          'scale' => 30,
          'unsigned' => TRUE,
          'not null' => FALSE,
          'default' => 0,
        ],
        'currency_code' => [
          'description' => new TranslatableMarkup('Letter currency code'),
          'type' => 'varchar',
          'length' => 3,
          'not null' => FALSE,
          'default' => '',
        ],
      ],
      'indexes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $isEmpty = FALSE;
    if (!$this->get('group')->getValue()) {
      $isEmpty = TRUE;
    }
    return $isEmpty;
  }

  /**
   * Return available fields array for display in template.
   */
  public static function getAvailableFields() {
    return [
      'group' => new TranslatableMarkup('Price group'),
      'number_from' => new TranslatableMarkup('Price from'),
      'number' => new TranslatableMarkup('Price'),
      'currency_code' => new TranslatableMarkup('Letter currency code'),
      'prefix' => new TranslatableMarkup('Price prefix'),
      'suffix' => new TranslatableMarkup('Price suffix'),
    ];
  }

}
