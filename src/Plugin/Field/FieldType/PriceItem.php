<?php

namespace Drupal\site_commerce_price\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the price field type.
 *
 * @FieldType(
 *   id = "site_commerce_price",
 *   label = @Translation("Price"),
 *   description = @Translation("Stores information about the decimal value of the price and currency code."),
 *   category = @Translation("SiteCommerce"),
 *   default_widget = "site_commerce_price_default",
 *   default_formatter = "site_commerce_price_default"
 * )
 */
class PriceItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    return [
      'available_currencies' => [],
      'all_currencies' => $currency_codes,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $currencies = \Drupal::entityTypeManager()->getStorage('site_commerce_currency')->loadMultiple();
    $currency_codes = array_keys($currencies);

    $element = [];
    $element['available_currencies'] = [
      '#type' => 'checkboxes',
      '#title' => new TranslatableMarkup('Available currencies'),
      '#description' => new TranslatableMarkup('If no currencies are selected, all currencies will be available.'),
      '#options' => array_combine($currency_codes, $currency_codes),
      '#default_value' => $this->getSetting('available_currencies'),
      '#multiple' => TRUE,
      '#size' => 5,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['number'] = DataDefinition::create('float')
      ->setLabel(t('Price'))
      ->setRequired(FALSE);

    $properties['currency_code'] = DataDefinition::create('string')
      ->setLabel(t('Currency'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'number' => [
          'description' => new TranslatableMarkup('Price'),
          'type' => 'numeric',
          'precision' => 65,
          'scale' => 30,
          'unsigned' => TRUE,
          'not null' => FALSE,
          'default' => 0,
        ],
        'currency_code' => [
          'description' => new TranslatableMarkup('Currency'),
          'type' => 'varchar',
          'length' => 3,
          'not null' => FALSE,
          'default' => '',
        ],
      ],
      'indexes' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->number === NULL || $this->number === '' || empty($this->currency_code);
  }

}
