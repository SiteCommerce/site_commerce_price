<?php

namespace Drupal\site_commerce_price\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the group price formatter.
 *
 * @FieldFormatter(
 *   id = "site_commerce_price_group_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_commerce_price_group"
 *   }
 * )
 */
class PriceGroupDefault extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    $values = [];

    $entity = $items->getEntity();

    foreach ($items as $delta => $item) {
      if ($item->group == site_commerce_product_get_user_price_group()) {
        $manager = \Drupal::entityTypeManager()
          ->getStorage('site_commerce_currency')
          ->loadByProperties(['letter_code' => $item->currency_code]);
        $currency = reset($manager);

          $values = [
          'group' => $item->group,
          'prefix' => $item->prefix,
          'suffix' => $item->suffix,
          'number_from' => \Drupal::service('kvantstudio.formatter')->price($item->number_from),
          'number' => \Drupal::service('kvantstudio.formatter')->price($item->number),
          'currency_code' => $item->currency_code,
          'symbol' => $currency->getSymbol(),
          'quantity_unit' => $entity->getQuantityUnit(),
        ];
      }
    }

    $elements = [
      '#theme' => 'site_commerce_price_group_default_formatter',
      '#data' => $values,
    ];

    return $elements;
  }

}
