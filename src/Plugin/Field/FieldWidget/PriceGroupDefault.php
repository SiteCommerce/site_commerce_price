<?php

namespace Drupal\site_commerce_price\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\site_commerce_price\Plugin\Field\FieldType\PriceGroupItem;

/**
 * Plugin implementation of the group price widget.
 *
 * @FieldWidget(
 *   id = "site_commerce_price_group_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_commerce_price_group"
 *   }
 * )
 */
class PriceGroupDefault extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = [];

    // Получаем перечень доступных полей.
    $options['available fields'] = [
      'group' => 'group',
      'number_from' => 0,
      'number' => 'number',
      'currency_code' => 'currency_code',
      'prefix' => 0,
      'suffix' => 0,
    ];

    return $options + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    // Получаем перечень доступных полей.
    $fields = PriceGroupItem::getAvailableFields();
    $options = array_keys($fields);

    $default_value = $this->getSetting('available fields');

    $element['available fields'] = [
      '#type' => 'checkboxes',
      '#required' => true,
      '#options' => array_combine($options, $fields),
      '#title' => t('Select available fields'),
      '#default_value' => $default_value,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $available_fields = array_diff($this->getSetting('available fields'), [0, NULL]);
    $summary[] = t('Available fields: @fields.', ['@fields' => count($available_fields)]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $available_fields = array_diff($this->getSetting('available fields'), [0, NULL]);

    $element['group'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Price group'),
      '#options' => ['' => new TranslatableMarkup('None'), 'retail' => new TranslatableMarkup('Retail buyer')],
      '#default_value' => isset($items[$delta]->group) ? $items[$delta]->group : 0,
      '#empty_value' => '',
      '#access' => in_array('group', $available_fields) ? TRUE : FALSE,
    ];

    $element['number_from'] = [
      '#type' => 'number',
      '#title' => new TranslatableMarkup('Price from'),
      '#default_value' => isset($items[$delta]->number_from) ? \Drupal::service('kvantstudio.formatter')->removeTrailingZeros($items[$delta]->number_from) : 0,
      '#empty_value' => '',
      '#step' => '0.01',
      '#min' => '0',
      '#access' => in_array('number_from', $available_fields) ? TRUE : FALSE,
    ];

    $element['number'] = [
      '#type' => 'number',
      '#title' => new TranslatableMarkup('Price'),
      '#default_value' => isset($items[$delta]->number) ? \Drupal::service('kvantstudio.formatter')->removeTrailingZeros($items[$delta]->number) : 0,
      '#empty_value' => '',
      '#step' => '0.01',
      '#min' => '0',
      '#access' => in_array('number', $available_fields) ? TRUE : FALSE,
    ];

    $available_currencies = array_filter($this->getFieldSetting('available_currencies'));
    if (!$available_currencies) {
      $available_currencies = array_filter($this->getFieldSetting('all_currencies'));
    }
    $element['currency_code'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Currency'),
      '#options' => array_combine($available_currencies, $available_currencies),
      '#default_value' => isset($items[$delta]->currency_code) ? $items[$delta]->currency_code : '',
      '#access' => in_array('currency_code', $available_fields) ? TRUE : FALSE,
    ];

    $element['prefix'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Price prefix'),
      '#default_value' => isset($items[$delta]->prefix) ? $items[$delta]->prefix : '',
      '#empty_value' => '',
      '#maxlength' => 255,
      '#access' => in_array('prefix', $available_fields) ? TRUE : FALSE,
    ];

    $element['suffix'] = [
      '#type' => 'textfield',
      '#title' => new TranslatableMarkup('Price suffix'),
      '#default_value' => isset($items[$delta]->suffix) ? $items[$delta]->suffix : '',
      '#empty_value' => '',
      '#maxlength' => 255,
      '#access' => in_array('suffix', $available_fields) ? TRUE : FALSE,
    ];

    $element['#theme'] = 'site_commerce_price_group_default_widget';

    return $element;
  }

}
