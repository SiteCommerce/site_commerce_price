<?php

namespace Drupal\site_commerce_price\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the group price widget.
 *
 * @FieldWidget(
 *   id = "site_commerce_price_default",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "site_commerce_price"
 *   }
 * )
 */
class PriceDefault extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['number'] = [
      '#type' => 'number',
      '#title' => new TranslatableMarkup('Price'),
      '#default_value' => isset($items[$delta]->number) ? \Drupal::service('kvantstudio.formatter')->removeTrailingZeros($items[$delta]->number) : 0,
      '#empty_value' => '',
      '#step' => '0.01',
      '#min' => '0',
      '#theme_wrappers' => [],
    ];

    $available_currencies = array_filter($this->getFieldSetting('available_currencies'));
    if (!$available_currencies) {
      $available_currencies = array_filter($this->getFieldSetting('all_currencies'));
    }
    $element['currency_code'] = [
      '#type' => 'select',
      '#title' => new TranslatableMarkup('Currency'),
      '#options' => array_combine($available_currencies, $available_currencies),
      '#default_value' => isset($items[$delta]->currency_code) ? $items[$delta]->currency_code : '',
      '#theme_wrappers' => [],
    ];

    return $element;
  }

}
