<?php

namespace Drupal\site_commerce_price\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the currency entity class.
 *
 * @ConfigEntityType(
 *   id = "site_commerce_currency",
 *   label = @Translation("Currency"),
 *   label_collection = @Translation("Currencies"),
 *   label_singular = @Translation("currency"),
 *   label_plural = @Translation("currencies"),
 *   label_count = @PluralTranslation(
 *     singular = "@count currency",
 *     plural = "@count currencies",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\site_commerce_price\Form\CurrencyForm",
 *       "edit" = "Drupal\site_commerce_price\Form\CurrencyForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\site_commerce_price\CurrencyRouteProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     },
 *     "list_builder" = "Drupal\site_commerce_price\CurrencyListBuilder",
 *   },
 *   admin_permission = "administer site_commerce_currency",
 *   config_prefix = "site_commerce_currency",
 *   entity_keys = {
 *     "id" = "letter_code",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "name",
 *     "letter_code",
 *     "numeric_code",
 *     "symbol",
 *     "fraction_digits"
 *   },
 *   links = {
 *     "add-form" = "/admin/site-commerce/config/currencies/add-custom",
 *     "edit-form" = "/admin/site-commerce/config/currencies/{site_commerce_currency}",
 *     "delete-form" = "/admin/site-commerce/config/currencies/{site_commerce_currency}/delete",
 *     "collection" = "/admin/site-commerce/config/currencies"
 *   }
 * )
 */
class Currency extends ConfigEntityBase implements CurrencyInterface {

  /**
   * The currency name.
   *
   * @var string
   */
  protected $name;

  /**
   * The alphanumeric currency code.
   *
   * @var string
   */
  protected $letter_code;

  /**
   * The numeric currency code.
   *
   * @var string
   */
  protected $numeric_code;

  /**
   * The currency symbol.
   *
   * @var string
   */
  protected $symbol;

  /**
   * The number of fraction digits.
   *
   * @var int
   */
  protected $fraction_digits = 2;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->letter_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getLetterCode() {
    return $this->letter_code;
  }

  /**
   * {@inheritdoc}
   */
  public function setLetterCode($letter_code) {
    $this->letter_code = $letter_code;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->name;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->name = $name;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getNumericCode() {
    return $this->numeric_code;
  }

  /**
   * {@inheritdoc}
   */
  public function setNumericCode($numeric_code) {
    $this->numeric_code = $numeric_code;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSymbol() {
    return $this->symbol;
  }

  /**
   * {@inheritdoc}
   */
  public function setSymbol($symbol) {
    $this->symbol = $symbol;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFractionDigits() {
    return $this->fraction_digits;
  }

  /**
   * {@inheritdoc}
   */
  public function setFractionDigits($fraction_digits) {
    $this->fraction_digits = $fraction_digits;
    return $this;
  }

}
